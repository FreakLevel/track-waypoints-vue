require 'test_helper'

class WaypointTest < ActiveSupport::TestCase
  test "Waypoint without values" do
    waypoint = Waypoint.new
    assert_not waypoint.save
  end

  test "Waypoint without latitude" do
    waypoint = Waypoint.new(longitude: 15, send_at: "2018-12-23 17:32:04", vehicles_id: 1)
    assert_not waypoint.save
  end
end
