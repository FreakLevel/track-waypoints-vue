require 'test_helper'

class VehicleTest < ActiveSupport::TestCase
  test "New Vehicle without identifier" do
    vehicle = Vehicles.new
    assert_not vehicle.save
  end

  test "New Vehicles with identifier" do
    vehicle = Vehicles.new(identifier: "GPS-123")
    assert vehicle.save
  end
end
