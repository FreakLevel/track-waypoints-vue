require 'test_helper'

class NewWaypointTest < ActionDispatch::IntegrationTest
  test "add waypoint" do
    post "/api/v1/gps",
      params: {
        "latitude": 20.23,
        "longitude": -0.56,
        "sent_at": "2016-06-02 20:45:00",
        "vehicle_identifier": "HA-3452"
      }
    assert_response :success
  end
end
