Rails.application.routes.draw do
  root 'vehicle#index'
  get 'show', action: :index, controller: 'vehicle', as: :show
  get 'show/:identifier', to: 'vehicle#show', as: :vehicle, constraint: { identifier: /\d/ }
  #post 'api/v1/gps', to: 'waypoint#create'
  get 'api/v1/waypoint/:vehicles_id', to: 'waypoint#get_last', constraint: { vehicles_id: /\d/ }
  get 'api/v1/waypoints/:vehicles_id', to: 'waypoint#get_historical', constraint: { vehicles_id: /\w\d/ }
  namespace 'api', defaults: { format: "json" } do
    namespace 'v1' do
      post 'gps', controller: 'gps', action: :create
    end
  end
end
