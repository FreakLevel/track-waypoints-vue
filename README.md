# About

This project was realized by Eduardo Tapia for postulation to job how backend developer on BeeTrack company.

The idea for this, is have a monitoring of vehicles associeted, giving the location using its coordinates, keeping a identifier of vehicle and record datetime of information; all this have a historical to the information passade and you can see them with the respective location and datetime of the moment.

# Technology

For this project was neccesary use Ruby programming language with RoR framework (Ruby and Rails) for make as web application; about the gems used, is installed pg, foreman, webpacker, delayed_job_active_record.

Gems:
- pg for make connection with postgresql database.
- foreman help to run project from Procfile file.
- webpacker for use webpack framework, this was necessary for can use vue framework to make the frontend.
- delayed_job_active_record allow make queues of jobs, helping to keep avaiblility despite have a high demand and flow of information.

Versions:
- Ruby: 2.6.0
- Rails: 5.2.2
- Vue: 2.5.22
- Postgresql: 9.5

Also need yarn how packages provider to install from package-json.

# Install

To install the project you need download.

``` bash
git clone https://gitlab.com/FreakLevel/track-waypoints-vue.git

cd track-waypoints-vue
```

You need change the database information in config/database.yml

After that you can follow.

```
bundle install

yarn install

bin/rails db:migrate
```

Now for run how local project need open de Procfile file and comment the worker line.

```
foreman start
```

And the project begin to load.

Using other console instance run a background worker executing:

```
bin/rake jobs:work
```

You are ready to open http://localhost:3000 to see the project or if you want [click here to see online.](https://track-waypoints.herokuapp.com/)

# Use

To use, browse the page and to send new information you need make a POST request.

```
/api/v1/gps
```

and add a json with the data using this structure.

```
{
  "latitude": -36.606560,
  "longitude": -72.086804,
  "sent_at": "2018-12-28 15:09:43",
  "vehicle_identifier": "ARS-902"
}
```

after a few moment you can see in the page you data uploaded.

# Acknowledgement

Thank you for the oportunity of make participe of this postulation and hope you like it.