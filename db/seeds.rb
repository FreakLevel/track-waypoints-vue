# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Vehicles.create([{identifier: 'RFG563', updated_at: '2019-01-25 18:29:34'}, {identifier: 'RLX291', updated_at: '2019-01-25 18:23:59'}])
Waypoint.create([{latitude: -33.546348, longitude: -70.569253, send_at: '2019-01-24 10:48:04', vehicles_id: 1}, {latitude: -32.528348, longitude: -70.599853, send_at: '2019-01-25 18:29:34', vehicles_id: 1}, {latitude: -33.546348, longitude: -70.599853, send_at: '2019-01-25 18:23:59', vehicles_id: 2},])