class AddNullableToVehicles < ActiveRecord::Migration[5.2]
  def change
    change_column :vehicles, :updated_at, :datetime, :null => true
  end
end
