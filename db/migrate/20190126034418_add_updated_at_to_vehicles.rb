class AddUpdatedAtToVehicles < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :updated_at, :datetime
  end
end
