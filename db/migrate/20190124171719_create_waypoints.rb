class CreateWaypoints < ActiveRecord::Migration[5.2]
  def change
    create_table :waypoints do |t|
      t.float :latitude
      t.float :longitude
      t.datetime :send_at
    end
    add_reference :waypoints, :vehicles, foreign_key: true
  end
end
