class AddIndexToVehicles < ActiveRecord::Migration[5.2]
  def change
    add_index :vehicles, :identifier, unique: true
  end
end
