class Waypoint < ApplicationRecord
    belongs_to :vehicles
    validates :latitude, presence: true
    validates :longitude, presence: true
    validates :vehicles_id, presence: true
    validates :send_at, presence: true
end
