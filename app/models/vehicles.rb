class Vehicles < ApplicationRecord
    has_many :waypoints, dependent: :destroy
    validates :identifier, presence: true

end
