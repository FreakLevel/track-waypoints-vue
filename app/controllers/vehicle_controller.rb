class VehicleController < ApplicationController

  before_action :set_vehicle, only: [:show]

  def index
    @vehicles = Vehicles.all
    current_uri = request.env['PATH_INFO']
    respond_to do |format|
      format.json {
        render json: { vehicles: @vehicles, uri: current_uri }
      }
      format.html
    end
  end

  def show()
    respond_to do |format|
      if vehicle_exist
        format.json {
          render json: { vehicle: vehicle_find, status: true }
        }
        format.html {
          @vehicle = vehicle_find
          @status= true 
        }
      else
        format.json {
          render json: { status: false }
        }
        format.html
      end
    end
  end

  class << self

    def create_or_find(identifier)
      Vehicles.find_or_create_by(identifier: identifier)
    end
  end

  private

    def set_vehicle
    end

    def vehicle_exist
      Vehicles.exists?(params[:identifier])
    end

    def vehicle_find
      Vehicles.find(params[:identifier])
    end

end
