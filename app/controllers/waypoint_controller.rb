class WaypointController < ApplicationController

    def get_last
        render json: last_waypoint
    end

    def get_historical
        render json: historical_waypoints
    end

    class << self
        def create_waypoint(params)
            message = {}
            vehicle = VehicleController.create_or_find(params[:vehicle_identifier])
            if vehicle
                vehicle.update(updated_at: Time.parse(params[:sent_at]).strftime("%Y-%m-%d %H:%M:%S"))
                if create(vehicle, params)
                    message = { status: true, time: Time.parse(params[:sent_at]).strftime("%Y-%m-%d %H:%M:%S") }
                end
            end
            return message
        end
        handle_asynchronously :create_waypoint

        def create(vehicle, params)
            Waypoint.create(latitude: params[:latitude], longitude: params[:longitude], send_at: Time.parse(params[:sent_at]).strftime("%Y-%m-%d %H:%M:%S"), vehicles_id: vehicle.id)
        end
    end

    private

    def last_waypoint
        Waypoint.order(send_at: :desc).where(vehicles_id: params[:vehicles_id]).first()
    end
    
    def historical_waypoints
        Waypoint.order(send_at: :desc).where(vehicles_id: params[:vehicles_id]).all()
    end
    
end
