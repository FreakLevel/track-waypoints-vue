module Api
    module V1
        class GpsController < ApplicationController
            skip_before_action :verify_authenticity_token
            def create
                message = WaypointController.create_waypoint(params)
                render json:  {job: message}, status: :ok
            end
        end
    end
end